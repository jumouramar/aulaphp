<?php

$numero1 = 2;
$numero2 = 3;

// Soma
echo $numero1 + $numero2;
echo "<br>";
// Subtração
echo $numero1 - $numero2;
echo "<br>";
// Divisão
echo $numero1 / $numero2;
echo "<br>";
// Multiplicação
echo $numero1 * $numero2;
echo "<br>";
// Potenciação
echo $numero1 ** $numero2;
echo "<br>";

?>

<br>

<?php

// Strings

$nome1 = "Alice";
$nome2 = "Bruno";

echo $nome1 . $nome2;
echo "<br>";

echo $nome1 . ", " . $nome2;
echo "<br>";

echo "$nome2, $nome1, $numero1";
echo "<br>";

echo "$nome2[0]";
echo "<br>";
echo $nome2[1];
echo "<br>";
echo "{$nome2[2]}";
echo "<br>";

?>

<br>

<?php

// Boolean
// == igualdade, mas não mesmo tipo
// === igualdade e mesmo tipo

$num1 = 1;
$num2 = 1.0;

echo ($num1 == $num2);
echo "<br>";
echo ($num1 === $num2);
echo "<br>";
?>

<br>

<?php

// > < >= <= !== !=== += -= ++ --

$num1 = 1;
$num2 = 1.0;

$num1 += 2;
echo $num1;
echo "<br>";
$num1--;
echo $num1;
?>