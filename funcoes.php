<?php

function msg($msg)
{
    echo $msg;
}

msg("Ola mundo!");

echo "<br>";
echo "<br>";

function soma($x, $y)
{
    return $x + $y;
}

echo soma(2, 3);

echo "<br>";
echo "<br>";

// passagem por referência
function soma5(&$x)
{
    $x += 5;
}

$variavel = 2;
echo $variavel . "<br>";
soma5($variavel);
echo $variavel . "<br>";

echo "<br>";
echo "<br>";

function soma_nova(int $x, int $y)
{
    return $x + $y;
}

echo soma_nova(2, 3) . "<br>";
// echo soma_nova(2, 3.5); // reclama

?>