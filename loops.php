<pre>

<?php

$produtos = [
    [
        'descricao' => 'Camisa Preta',
        'preco' => '51'
    ],
    [
        'descricao' => 'Camisa Branca',
        'preco' => '58'
    ],
    [
        'descricao' => 'Calça Jeans',
        'preco' => '101'
    ]
];

// print_r($produtos);

for($i=0; $i < count($produtos); $i++){
    print_r($produtos[$i]);
}

echo "<br>";
echo "<br>";
echo "<br>";

foreach($produtos as $produto){
    // print_r($produto);
    echo $produto['descricao']."<br>";
    echo $produto['preco']."<br>";
}

echo "<br>";
echo "<br>";
echo "<br>";

$contador = 0;

while($contador < count($produtos)){
    print_r($produtos[$contador]);
    $contador++;
}

echo "<br>";
echo "<br>";
echo "<br>";

foreach($produtos as $key => $produto){
    echo $key."<br>";
    foreach($produto as $key => $value){
        echo $key . " -> ";
        echo $value . "<br>";
    }
}

?>

</pre>