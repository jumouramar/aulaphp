<pre> 

<?php
$produtos = array('camiseta', 'bermuda', 'calça');
// $mix = array('camiseta', 3, false); // não é uma boa prática
$produtos1 = ['tenis', 'oculos', 'saia'];

print_r($produtos);
echo "<br>";
var_dump($produtos);
echo "<br>";
echo ($produtos[1]);
// echo ($produtos[4]); // erro
echo "<br>";
echo count($produtos);
echo "<br>";
echo "<br>";


$produtos2 = ['tenis', 'oculos', ['saia', 'meia']];
var_dump($produtos2);


$produtos3 = [
    'descrição' => 'Camiseta Preta',
    'preço' => 51.0,
    'img' => [
        'src' => 'img/imagem.png',
        'alt' => 'Camisa Preta'
    ]
];
print_r($produtos3);
echo "<br>";
echo $produtos3['descrição'];
echo "<br>";
echo $produtos3['img']['src'];
echo "<br>";
echo "<br>";

// unset - retirar elemento
unset($produtos2[1]);
print_r($produtos2);
echo "<br>";
unset($produtos3['img']);
print_r($produtos3);
echo "<br>";
// criando novo elemento
$produtos3['estoque'] = 10; 
print_r($produtos3);

?>

</pre>